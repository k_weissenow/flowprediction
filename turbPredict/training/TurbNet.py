#!/usr/bin/python

import torch
import torch.nn as nn
import torch.nn.functional as F

def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

def blockUNet(in_c, out_c, name, transposed=False, bn=True, relu=True, dropout=False):
    block = nn.Sequential()
    if relu:
        block.add_module('%s.relu' % name, nn.ReLU(inplace=True))
    else:
        block.add_module('%s.leakyrelu' % name, nn.LeakyReLU(0.2, inplace=True))
    if not transposed:
        block.add_module('%s.conv' % name, nn.Conv2d(in_c, out_c, 4, 2, 1, bias=False))
    else:
        block.add_module('%s.tconv' % name, nn.ConvTranspose2d(in_c, out_c, 4, 2, 1, bias=False))
    if bn:
        block.add_module('%s.bn' % name, nn.BatchNorm2d(out_c))
    if dropout:
        block.add_module('%s.dropout' % name, nn.Dropout2d(0.5, inplace=True))
    return block

# Discriminator (2.7m weights)
class TurbNetD(nn.Module):
    def __init__(self, inputChannels=6):
        super(TurbNetD, self).__init__()

        self.main = nn.Sequential()

        self.main.add_module('layer1.conv', nn.Conv2d(inputChannels, 64, 4, 2, 1, bias=False))
        self.main.add_module('layer2', blockUNet(64, 128, 'layer2', transposed=False, bn=True, relu=False, dropout=False))
        self.main.add_module('layer3', blockUNet(128, 256, 'layer3', transposed=False, bn=True, relu=False, dropout=False))
        
        self.main.add_module('layer4.leakyrelu', nn.LeakyReLU(0.2, inplace=True))
        self.main.add_module('layer4.conv', nn.Conv2d(256, 512, 4, 1, 1, bias=False))
        self.main.add_module('layer4.bn', nn.BatchNorm2d(512))

        self.main.add_module('layer5.leakyrelu', nn.LeakyReLU(0.2, inplace=True))
        self.main.add_module('layer5.conv', nn.Conv2d(512, 1, 4, 1, 1, bias=False))
        self.main.add_module('layer5.sigmoid', nn.Sigmoid())

    def forward(self, x):
        output = self.main(x)
        return output

# Extended discriminator (24.3m weights)
class TurbNetD_extended(nn.Module):
    def __init__(self):
        super(TurbNetD_extended, self).__init__()

        self.main = nn.Sequential()

        self.main.add_module('layer1.conv', nn.Conv2d(6, 64, 4, 2, 1, bias=False))
        self.main.add_module('layer2', blockUNet(64, 128, 'layer2', transposed=False, bn=True, relu=False, dropout=False))
        self.main.add_module('layer3', blockUNet(128, 256, 'layer3', transposed=False, bn=True, relu=False, dropout=False))

        self.main.add_module('layer4.leakyrelu', nn.LeakyReLU(0.2, inplace=True))
        self.main.add_module('layer4.conv', nn.Conv2d(256, 512, 3, 1, 1, bias=False))
        self.main.add_module('layer4.bn', nn.BatchNorm2d(512))

        self.main.add_module('layer5.leakyrelu', nn.LeakyReLU(0.2, inplace=True))
        self.main.add_module('layer5.conv', nn.Conv2d(512, 1024, 3, 1, 1, bias=False))
        self.main.add_module('layer5.bn', nn.BatchNorm2d(1024))

        self.main.add_module('layer5x.leakyrelu', nn.LeakyReLU(0.2, inplace=True))
        self.main.add_module('layer5x.conv', nn.Conv2d(1024, 1024, 3, 1, 1, bias=False))
        self.main.add_module('layer5x.bn', nn.BatchNorm2d(1024))
        
        self.main.add_module('layer6.leakyrelu', nn.LeakyReLU(0.2, inplace=True))
        self.main.add_module('layer6.conv', nn.Conv2d(1024, 512, 4, 1, 1, bias=False))
        self.main.add_module('layer6.bn', nn.BatchNorm2d(512))

        self.main.add_module('layer7.leakyrelu', nn.LeakyReLU(0.2, inplace=True))
        self.main.add_module('layer7.conv', nn.Conv2d(512, 1, 4, 1, 1, bias=False))
        self.main.add_module('layer7.sigmoid', nn.Sigmoid())

    def forward(self, x):
        output = self.main(x)
        return output

# Generator (29.2m weights)
class TurbNetG(nn.Module):
    def __init__(self, channelExponent=6):
        super(TurbNetG, self).__init__()

        # weights per channelExponent:
        # 2:    114,560
        # 3:    457,472
        # 4:  1,828,352
        # 5:  7,310,336
        # 6: 29,235,200

        channels = 2 ** channelExponent

        self.layer1 = nn.Sequential()
        self.layer1.add_module('layer1.conv', nn.Conv2d(3, channels, 4, 2, 1, bias=False))

        self.layer2 = blockUNet(channels, channels*2, 'layer2', transposed=False, bn=True, relu=False, dropout=False)
        self.layer3 = blockUNet(channels*2, channels*4, 'layer3', transposed=False, bn=True, relu=False, dropout=False)
        self.layer4 = blockUNet(channels*4, channels*8, 'layer4', transposed=False, bn=True, relu=False, dropout=False)
        self.layer5 = blockUNet(channels*8, channels*8, 'layer5', transposed=False, bn=True, relu=False, dropout=False)
        self.layer6 = blockUNet(channels*8, channels*8, 'layer6', transposed=False, bn=False, relu=False, dropout=False)

        self.dlayer6 = blockUNet(channels*8, channels*8, 'dlayer6', transposed=True, bn=True, relu=True, dropout=True)
        self.dlayer5 = blockUNet(channels*16, channels*8, 'dlayer5', transposed=True, bn=True, relu=True, dropout=True)
        self.dlayer4 = blockUNet(channels*16, channels*4, 'dlayer4', transposed=True, bn=True, relu=True, dropout=True)
        self.dlayer3 = blockUNet(channels*8, channels*2, 'dlayer3', transposed=True, bn=True, relu=True, dropout=False)
        self.dlayer2 = blockUNet(channels*4, channels, 'dlayer2', transposed=True, bn=True, relu=True, dropout=False)

        self.dlayer1 = nn.Sequential()
        self.dlayer1.add_module('dlayer1.relu', nn.ReLU(inplace=True))
        self.dlayer1.add_module('dlayer1.tconv', nn.ConvTranspose2d(channels*2, 3, 4, 2, 1, bias=False))
        self.dlayer1.add_module('dlayer1.tanh', nn.Tanh())

    def forward(self, x):
        out1 = self.layer1(x)
        out2 = self.layer2(out1)
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        out5 = self.layer5(out4)
        out6 = self.layer6(out5)
        dout6 = self.dlayer6(out6)
        dout6_out5 = torch.cat([dout6, out5], 1)
        dout5 = self.dlayer5(dout6_out5)
        dout5_out4 = torch.cat([dout5, out4], 1)
        dout4 = self.dlayer4(dout5_out4)
        dout4_out3 = torch.cat([dout4, out3], 1)
        dout3 = self.dlayer3(dout4_out3)
        dout3_out2 = torch.cat([dout3, out2], 1)
        dout2 = self.dlayer2(dout3_out2)
        dout2_out1 = torch.cat([dout2, out1], 1)
        dout1 = self.dlayer1(dout2_out1)
        return dout1

# Alternative generator (16.6m weights)
class TurbNetG_reduced(nn.Module):
    def __init__(self, channelExponent=6):
        super(TurbNetG_reduced, self).__init__()

        # weights per channelExponent:
        # 2:     65,408
        # 3:    260,864
        # 4:  1,041,920
        # 5:  4,164,608
        # 6: 16,652,288

        channels = 2 ** channelExponent

        self.layer1 = nn.Sequential()
        self.layer1.add_module('layer1.conv', nn.Conv2d(3, channels, 4, 2, 1, bias=False))

        self.layer2 = blockUNet(channels, channels*2, 'layer2', transposed=False, bn=True, relu=False, dropout=False)
        self.layer3 = blockUNet(channels*2, channels*4, 'layer3', transposed=False, bn=True, relu=False, dropout=False)
        self.layer4 = blockUNet(channels*4, channels*8, 'layer4', transposed=False, bn=True, relu=False, dropout=False)
        self.layer5 = blockUNet(channels*8, channels*8, 'layer5', transposed=False, bn=True, relu=False, dropout=False)

        self.dlayer5 = blockUNet(channels*8, channels*8, 'dlayer5', transposed=True, bn=True, relu=True, dropout=True)
        self.dlayer4 = blockUNet(channels*16, channels*4, 'dlayer4', transposed=True, bn=True, relu=True, dropout=True)
        self.dlayer3 = blockUNet(channels*8, channels*2, 'dlayer3', transposed=True, bn=True, relu=True, dropout=False)
        self.dlayer2 = blockUNet(channels*4, channels, 'dlayer2', transposed=True, bn=True, relu=True, dropout=False)

        self.dlayer1 = nn.Sequential()
        self.dlayer1.add_module('dlayer1.relu', nn.ReLU(inplace=True))
        self.dlayer1.add_module('dlayer1.tconv', nn.ConvTranspose2d(channels*2, 3, 4, 2, 1, bias=False))
        self.dlayer1.add_module('dlayer1.tanh', nn.Tanh())

    def forward(self, x):
        out1 = self.layer1(x)
        out2 = self.layer2(out1)
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        out5 = self.layer5(out4)
        dout5 = self.dlayer5(out5)
        dout5_out4 = torch.cat([dout5, out4], 1)
        dout4 = self.dlayer4(dout5_out4)
        dout4_out3 = torch.cat([dout4, out3], 1)
        dout3 = self.dlayer3(dout4_out3)
        dout3_out2 = torch.cat([dout3, out2], 1)
        dout2 = self.dlayer2(dout3_out2)
        dout2_out1 = torch.cat([dout2, out1], 1)
        dout1 = self.dlayer1(dout2_out1)
        return dout1

# Alternative generator (6.1m weights)
class TurbNetG_shallow(nn.Module):
    def __init__(self):
        super(TurbNetG_shallow, self).__init__()

        self.layer1 = nn.Sequential()
        self.layer1.add_module('layer1.conv', nn.Conv2d(3, 64, 4, 2, 1, bias=False))

        self.layer2 = blockUNet(64, 128, 'layer2', transposed=False, bn=True, relu=False, dropout=False)
        self.layer3 = blockUNet(128, 256, 'layer3', transposed=False, bn=True, relu=False, dropout=False)
        self.layer4 = blockUNet(256, 512, 'layer4', transposed=False, bn=True, relu=False, dropout=False)

        self.dlayer4 = blockUNet(512, 256, 'dlayer4', transposed=True, bn=True, relu=True, dropout=True)
        self.dlayer3 = blockUNet(512, 128, 'dlayer3', transposed=True, bn=True, relu=True, dropout=False)
        self.dlayer2 = blockUNet(256, 64, 'dlayer2', transposed=True, bn=True, relu=True, dropout=False)

        self.dlayer1 = nn.Sequential()
        self.dlayer1.add_module('dlayer1.relu', nn.ReLU(inplace=True))
        self.dlayer1.add_module('dlayer1.tconv', nn.ConvTranspose2d(128, 3, 4, 2, 1, bias=False))
        self.dlayer1.add_module('dlayer1.tanh', nn.Tanh())

    def forward(self, x):
        out1 = self.layer1(x)
        out2 = self.layer2(out1)
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        dout4 = self.dlayer4(out4)
        dout4_out3 = torch.cat([dout4, out3], 1)
        dout3 = self.dlayer3(dout4_out3)
        dout3_out2 = torch.cat([dout3, out2], 1)
        dout2 = self.dlayer2(dout3_out2)
        dout2_out1 = torch.cat([dout2, out1], 1)
        dout1 = self.dlayer1(dout2_out1)
        return dout1

# Purely decoding generator
class TurbNetG_decoder(nn.Module):
    def __init__(self, channelExponent=6):
        super(TurbNetG_decoder, self).__init__()

        # weights per channelExponent:
        # 2:     47,808
        # 3:    182,656
        # 4:    713,472
        # 5:  2,719,584
        # 6: 11,209,728

        channels = 2 ** channelExponent

        self.dlayer6 = nn.Sequential()
        self.dlayer6.add_module('dlayer6.tconv', nn.ConvTranspose2d(4, channels*16, 4, 2, 1, bias=False))

        self.dlayer5 = blockUNet(channels * 16, channels * 8, 'dlayer5', transposed=True, bn=True, relu=True, dropout=True)
        self.dlayer4 = blockUNet(channels * 8, channels * 4, 'dlayer4', transposed=True, bn=True, relu=True, dropout=True)
        self.dlayer3 = blockUNet(channels * 4, channels * 2, 'dlayer3', transposed=True, bn=True, relu=True, dropout=False)
        self.dlayer2 = blockUNet(channels * 2, channels, 'dlayer2', transposed=True, bn=True, relu=True, dropout=False)

        self.dlayer1 = nn.Sequential()
        self.dlayer1.add_module('dlayer1.relu', nn.ReLU(inplace=True))
        self.dlayer1.add_module('dlayer1.tconv', nn.ConvTranspose2d(channels, 3, 4, 2, 1, bias=False))
        self.dlayer1.add_module('dlayer1.tanh', nn.Tanh())

    def forward(self, x):
        dout6 = self.dlayer6(x)
        dout5 = self.dlayer5(dout6)
        dout4 = self.dlayer4(dout5)
        dout3 = self.dlayer3(dout4)
        dout2 = self.dlayer2(dout3)
        dout1 = self.dlayer1(dout2)
        return dout1
