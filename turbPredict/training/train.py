################
# train.py
#
# Main model training script
################

import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt
import os
import random
from sys import maxsize
from TurbNet import TurbNetD, TurbNetG, weights_init
from TurbDataset import TurbDataset
from utils import imageOut, saveAsImage

######## Settings ########
# amount of epochs
epochs = 400
# use dropout during validation
validationDropout = False
# batch size
batch_size = 10
# discriminator output patch size (PatchGAN)
patchGANsize = 6
# L1 weight
lambda_L1 = 1.0
# GAN weight
lambda_LcGAN = 0.0
# Divergence loss
lambda_div = 0.0
# learning rate, generator
lrG = 0.0002
# learning rate, discriminator
lrD = 0.0002
# adam beta
beta = 0.5
# regularization strength
reg = 0.0
# enable cuda
cuda = True
##########################

def log(file, line):
    f = open(file, "a+")
    f.write(line)
    f.close()

losses = np.zeros(epochs)
validationAccuracies = np.zeros(epochs)
trainAccuracies = np.zeros(epochs)

seed = random.randint(0, maxsize)
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)
print("Seed: {}".format(seed))

netG = TurbNetG()
print(netG)
netG.apply(weights_init)
#netG.load_state_dict(torch.load("G_pretrain.model"))
netG.cuda()

netD = TurbNetD()
print(netD)
netD.apply(weights_init)
netD.cuda()

dataset = TurbDataset()
trainLoader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
testLoader = DataLoader(TurbDataset(train=False), batch_size=batch_size, shuffle=False)
print("Training batches: {}".format(len(trainLoader)))
print("Test batches: {}".format(len(testLoader)))

criterionCrossEntropy = nn.BCELoss()
criterionCrossEntropy.cuda()
criterionL1 = nn.L1Loss()
criterionL1.cuda()
optimizerG = optim.Adam(netG.parameters(), lr=lrG, betas=(beta, 0.999), weight_decay=0.0)
optimizerD = optim.SGD(netD.parameters(), lr=lrD)

targets = torch.FloatTensor(batch_size, 3, 64, 64)
targets = Variable(targets)
targets = targets.cuda()
inputs = torch.FloatTensor(batch_size, 3, 64, 64)
inputs = Variable(inputs)
inputs = inputs.cuda()
label_d = torch.FloatTensor(batch_size)
label_d = Variable(label_d)
label_d = label_d.cuda()
real_label = 1
fake_label = 0
divTarget = torch.FloatTensor(batch_size, 62, 62)
divTarget = Variable(divTarget)
divTarget = divTarget.cuda()

L_G_list = list()
L_D_list = list()
D_x_list = list()
D_G_xz1_list = list()
D_G_xz2_list = list()
L1_list = list()
div_list = list()

for epoch in range(epochs):
    print("Starting epoch {}".format(epoch))

    netG.train()
    L_G_accum = 0.0
    L_D_accum = 0.0
    D_x_accum = 0.0
    D_G_xz1_accum = 0.0
    D_G_xz2_accum = 0.0
    L1_accum = 0.0
    div_accum = 0.0
    samples_accum = 0
    for i, data in enumerate(trainLoader, 0):
        inputs_cpu, targets_cpu = data
        current_batch_size = targets_cpu.size(0)

        targets_cpu, inputs_cpu = targets_cpu.float().cuda(), inputs_cpu.float().cuda()
        inputs.data.resize_as_(inputs_cpu).copy_(inputs_cpu)
        targets.data.resize_as_(targets_cpu).copy_(targets_cpu)

        ###########################
        # discriminator training

        for p in netD.parameters():
            p.requires_grad = True
        netD.zero_grad()

        # real data
        label_d.data.resize_((current_batch_size, 1, patchGANsize, patchGANsize)).fill_(real_label)
        output = netD(torch.cat([targets, inputs], 1))
        Derror_real = criterionCrossEntropy(output, label_d)
        Derror_real.backward()
        D_x = output.data.mean()

        # fake data
        gen_out = netG(inputs)
        fake = gen_out.detach()
        label_d.data.fill_(fake_label)
        output = netD(torch.cat([fake, inputs], 1))
        Derror_fake = criterionCrossEntropy(output, label_d)
        Derror_fake.backward()
        D_G_xz = output.data.mean()
        Derror = Derror_fake + Derror_real
        optimizerD.step()

        ###########################
        # generator training

        for p in netD.parameters():
            p.requires_grad = False
        netG.zero_grad()

        lossL1 = criterionL1(gen_out, targets)
        lossL1viz = lossL1.data[0]
        lossL1 = lossL1 * lambda_L1
        lossL1.backward(retain_variables=True)

        gen_out_denormalized = dataset.denormalize_noCopy(gen_out)
        divergence = gen_out_denormalized[:, 1, 2:, 1:-1] - gen_out_denormalized[:, 1, :-2, 1:-1] + gen_out_denormalized[:, 2, 1:-1, 2:] - gen_out_denormalized[:, 2, 1:-1, :-2]
        divTarget.data.fill_(0)
        divLoss = criterionL1(divergence, divTarget)
        divLoss = divLoss * lambda_div
        divLoss.backward(retain_variables=True)

        label_d.data.fill_(real_label)
        output = netD(torch.cat([gen_out, inputs], 1))
        GerrorD = criterionCrossEntropy(output, label_d) * lambda_LcGAN
        GerrorD.backward()
        Gerror = output.data.mean()
        optimizerG.step()

        logline = "Epoch: {} Batch-idx: {} D(x): {} D(G(x,z))#1: {} D(G(x,z))#1: {}  L1: {} L_G: {} L_D: {} divLoss: {}\n".format(epoch, i, D_x, D_G_xz, Gerror, lossL1viz, GerrorD.data[0], Derror.data[0], divLoss.data[0])
        print(logline)
        #log("log.txt", logline)

        L_G_accum += GerrorD.data[0]
        L_D_accum += Derror.data[0]
        D_x_accum += D_x
        D_G_xz1_accum += D_G_xz
        D_G_xz2_accum += Gerror
        L1_accum += lossL1viz
        div_accum += divLoss.data[0]
        samples_accum += current_batch_size

    #torch.save(netG.state_dict(), "./model_G")
    #torch.save(netD.state_dict(), "./model_D")

    # validation
    if not validationDropout:
        netG.eval()
    L1val_accum = 0.0
    for i, data in enumerate(testLoader, 0):
        inputs_cpu, targets_cpu = data
        current_batch_size = targets_cpu.size(0)

        targets_cpu, inputs_cpu = targets_cpu.float().cuda(), inputs_cpu.float().cuda()
        inputs.data.resize_as_(inputs_cpu).copy_(inputs_cpu)
        targets.data.resize_as_(targets_cpu).copy_(targets_cpu)

        outputs = netG(inputs)
        outputs_cpu = outputs.data.cpu().numpy()

        lossL1 = criterionL1(outputs, targets)
        L1val_accum += lossL1.data[0]

        if i==0:
            outputs_denormalized = dataset.denormalize(outputs_cpu[0])
            targets_denormalized = dataset.denormalize(targets_cpu.cpu().numpy()[0])
            imageOut("results/epoch{}_{}".format(epoch, i), outputs_denormalized, targets_denormalized, saveTargets=True)

    # plotting
    L_G_accum /= len(trainLoader)
    log("L_G.txt", "{} ".format(L_G_accum))
    L_D_accum /= len(trainLoader)
    log("L_D.txt", "{} ".format(L_D_accum))
    D_x_accum /= len(trainLoader)
    log("D_x.txt", "{} ".format(D_x_accum))
    D_G_xz1_accum /= len(trainLoader)
    log("D_G_xz1.txt", "{} ".format(D_G_xz1_accum))
    D_G_xz2_accum /= len(trainLoader)
    log("D_G_xz2.txt", "{} ".format(D_G_xz2_accum))
    L1_accum /= len(trainLoader)
    log("L1.txt", "{} ".format(L1_accum))
    L1val_accum /= len(testLoader)
    log("L1val.txt", "{} ".format(L1val_accum))
    div_accum /= len(trainLoader)
    log("divLoss.txt", "{} ".format(div_accum))

torch.save(netG.state_dict(), "./modelG")
