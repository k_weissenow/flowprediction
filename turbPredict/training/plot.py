################
# plot.py
#
# Plots training curves of the last training run
################

import numpy as np
import matplotlib.pyplot as plt

L1 = np.loadtxt("L1.txt")
L1val = np.loadtxt("L1val.txt")
D_x = np.loadtxt("D_x.txt")
D_G_xz = np.loadtxt("D_G_xz1.txt")
L_D = np.loadtxt("L_D.txt")
L_G = np.loadtxt("L_G.txt")
div = np.loadtxt("divLoss.txt")

f, ax = plt.subplots(2, sharex=True)
ax[0].plot(L_D, color="b")
ax[0].set_xlabel('epoch')
ax[0].set_ylabel('Loss D')
ax[1].plot(L_G, color="r")
ax[1].set_xlabel('epoch')
ax[1].set_ylabel('Loss G')
plt.savefig("visualization/GANloss.png")

f, ax = plt.subplots(1)
ax.plot(D_x, color="b")
ax.plot(D_G_xz, color="r")
ax.set_xlabel('epoch')
ax.set_ylabel('label')
plt.savefig("visualization/GANdiscriminator.png")

f, ax = plt.subplots(1)
ax.plot(L1)
ax.plot(L1val)
ax.set_xlabel('epoch')
ax.set_ylabel('L1 loss')
plt.savefig("visualization/GAN_L1loss.png")

f, ax = plt.subplots(1)
ax.plot(div)
ax.set_xlabel('epoch')
ax.set_ylabel('div loss')
plt.savefig("visualization/GAN_divloss.png")
