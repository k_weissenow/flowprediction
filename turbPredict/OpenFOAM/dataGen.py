from random import *
import uuid
import os
import numpy as np

# parameters
cells = 64
nrSamples = 1000
dataDir = "../generatedData/"

def genFiles(cells, xVelTop, xVelBot, yVelLeft, yVelRight):
    with open("blockMeshDict_template", "rt") as inFile:
        with open("system/blockMeshDict", "wt") as outFile:
            for line in inFile:
                line = line.replace("CELLS", "{}".format(cells))
                outFile.write(line)
    with open("U_template", "rt") as inFile:
        with open("0/U", "wt") as outFile:
            for line in inFile:
                line = line.replace("X_VEL_TOP", "{}".format(xVelTop))
                line = line.replace("X_VEL_BOT", "{}".format(xVelBot))
                line = line.replace("Y_VEL_LEFT", "{}".format(yVelLeft))
                line = line.replace("Y_VEL_RIGHT", "{}".format(yVelRight))
                outFile.write(line)

def runSimulation():
    os.system("./Allclean && blockMesh > blockMesh.log && simpleFoam > foam.log")

def parseOutputFile(fileName):
    sampleList = []
    with open(fileName, "rt") as outFile:
        inList = False
        for line in outFile:
            if not inList:
                if line[0] == '(':
                    inList = True
                    continue
            else:
                if line[0] == ")":
                    break
                line = line.strip("()\n")
                lineElements = line.split()
                if len(lineElements) == 1:
                    sampleList.append(float(lineElements[0]))
                elif len(lineElements) == 3:
                    elementList = []
                    elementList.append(float(lineElements[0]))
                    elementList.append(float(lineElements[1]))
                    sampleList.append(elementList)
    if len(sampleList) > 0:
        return np.array(sampleList)
    else:
        return None

def saveArrays(dataDir, xVelTop, xVelBot, yVelLeft, yVelRight):
    # check for output directory
    outDirNum = 1500
    while not os.path.isdir(str(outDirNum)) and outDirNum > 0:
        outDirNum -= 1
    if outDirNum == 0:
        print("Error while looking for output directory! Did the simulation crash?")
        return False

    print("Output directory: {}".format(outDirNum))
    velData = parseOutputFile(str(outDirNum) + "/U")
    velData = velData.swapaxes(0,1).reshape(2, cells, cells)
    pressureData = parseOutputFile(str(outDirNum) + "/p")
    pressureData = pressureData.reshape(1, cells, cells)
    Y = np.concatenate((pressureData, velData))

    X = np.zeros((3, cells, cells))
    X[1,0] = xVelTop
    X[1,cells-1] = xVelBot
    X[2,:,0] = yVelLeft
    X[2,:,cells-1] = yVelRight

    X = X.reshape(1, 3, cells, cells)
    Y = Y.reshape(1, 3, cells, cells)
    combined = np.concatenate((X, Y))

    fileName = dataDir + str(uuid.uuid4())
    print("Saving in " + fileName + ".npz")
    np.savez_compressed(fileName, a=combined)
    
    return True

for i in range(nrSamples):
    # generate simulation files
    xVelTop = (random() - 0.5) * 4.0
    xVelBot = (random() - 0.5) * 4.0
    yVelLeft = (random() - 0.5) * 4.0
    yVelRight = (random() - 0.5) * 4.0
    genFiles(cells, xVelTop, xVelBot, yVelLeft, yVelRight)

    # run simulation and save data
    print("Running simulation..")
    runSimulation()
    if not saveArrays(dataDir, xVelTop, xVelBot, yVelLeft, yVelRight):
        break
    print("\n")
