********************************************************************
* Files for flow prediction featuring a closed box with moving walls
********************************************************************

* Data generation:
The directory 'OpenFOAM' contains the case files for the underlying OpenFOAM simulation. Simply run the script 'dataGen.py' to generate samples.
Output files are saved as compressed numpy arrays. The tensor size in each sample file is 6x64x64, where the first three channels represent the input pressure and velocity boundary conditions in x and y direction in that order. The other three channels contain the target fields after the simulation run, again using one pressure and two velocity channels.

* Network training:
The main training script in the 'training' directory is 'train.py'. 'plot.py' is used to plot graphs based on the log files of the most recent training run. The script 'validation.py' is used to generate output images from the validation set based on a pre-trained model (which is automatically saved by the training script at the end of the training).

The network definitions in 'TurbNet.py' include some legacy variants from early experimentation with GANs. The main architecture used is 'TurbNetG'.
