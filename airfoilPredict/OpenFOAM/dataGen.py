import numpy as np
import os
import uuid
from utils import saveAsImage

# parameters
airfoil_database = "../airfoil_database/"
output_dir = "../airfoil_generatedData/"
samples = 1000
freestreamX_min = 10.0
freestreamX_max = 100.0
freestreamY_min = -5.0
freestreamY_max = 5.0

def genMesh(airfoilFile):
    ar = np.loadtxt(airfoilFile, skiprows=1)

    # removing duplicate end point
    if np.max(np.abs(ar[0] - ar[(ar.shape[0]-1)]))<1e-6:
        ar = ar[:-1]

    output = ""
    pointIndex = 1000
    for n in range(ar.shape[0]):
        output += "Point({}) = {{ {}, {}, 0.00000000, 0.005}};\n".format(pointIndex, ar[n][0], ar[n][1])
        pointIndex += 1

    with open("airfoil_template.geo", "rt") as inFile:
        with open("airfoil.geo", "wt") as outFile:
            for line in inFile:
                line = line.replace("POINTS", "{}".format(output))
                line = line.replace("LAST_POINT_INDEX", "{}".format(pointIndex-1))
                outFile.write(line)

    if os.system("gmsh airfoil.geo -3 -o airfoil.msh > /dev/null") != 0:
        print('\033[91m' + "\terror during mesh creation!" + '\033[0m')
        return -1

    if os.system("gmshToFoam airfoil.msh > /dev/null") != 0:
        print('\033[91m' + "\terror during conversion to OpenFoam mesh!" + '\033[0m')
        return -1

    with open("constant/polyMesh/boundary", "rt") as inFile:
        with open("constant/polyMesh/boundaryTemp", "wt") as outFile:
            inBlock = False
            inAerofoil = False
            for line in inFile:
                if "front" in line or "back" in line:
                    inBlock = True
                elif "aerofoil" in line:
                    inAerofoil = True
                if inBlock and "type" in line:
                    line = line.replace("patch", "empty")
                    inBlock = False
                if inAerofoil and "type" in line:
                    line = line.replace("patch", "wall")
                    inAerofoil = False
                outFile.write(line)
    os.rename("constant/polyMesh/boundaryTemp","constant/polyMesh/boundary")

    return 0

def runSim(freestreamX, freestreamY):
    with open("U_template", "rt") as inFile:
        with open("0/U", "wt") as outFile:
            for line in inFile:
                line = line.replace("VEL_X", "{}".format(freestreamX))
                line = line.replace("VEL_Y", "{}".format(freestreamY))
                outFile.write(line)

    os.system("./Allclean && simpleFoam > foam.log")

def outputProcessing(freestreamX, freestreamY, dataDir=output_dir, pfile='postProcessing/internalCloud/500/cloud_p.xy', ufile='postProcessing/internalCloud/500/cloud_U.xy', res=128, imageIndex=0):

    # output layout:
    # [0] freestream field X + boundary
    # [1] freestream field Y + boundary
    # [2] binary mask for boundary
    # [3] pressure output
    # [4] velocity X output
    # [5] velocity Y output
    npOutput = np.zeros((6, res, res))

    ar = np.loadtxt(pfile)
    curIndex = 0

    for y in range(res):
        for x in range(res):
            xf = (x / res - 0.5) * 2 + 0.5
            yf = (y / res - 0.5) * 2
            if abs(ar[curIndex][0] - xf)<1e-4 and abs(ar[curIndex][1] - yf)<1e-4:
                npOutput[3][x][y] = ar[curIndex][3]
                curIndex += 1
                # fill input as well
                npOutput[0][x][y] = freestreamX
                npOutput[1][x][y] = freestreamY
            else:
                npOutput[3][x][y] = 0
                # fill mask
                npOutput[2][x][y] = 1.0


    ar = np.loadtxt(ufile)
    curIndex = 0

    for y in range(res):
        for x in range(res):
            xf = (x / res - 0.5) * 2 + 0.5
            yf = (y / res - 0.5) * 2
            if abs(ar[curIndex][0] - xf)<1e-4 and abs(ar[curIndex][1] - yf)<1e-4:
                npOutput[4][x][y] = ar[curIndex][3]
                npOutput[5][x][y] = ar[curIndex][4]
                curIndex += 1
            else:
                npOutput[4][x][y] = 0
                npOutput[5][x][y] = 0

    saveAsImage('pictures/{}_pressure.png'.format(imageIndex), npOutput[3])
    saveAsImage('pictures/{}_velX.png'.format(imageIndex), npOutput[4])
    saveAsImage('pictures/{}_velY.png'.format(imageIndex), npOutput[5])
    #saveAsImage('inputX{}.png'.format(imageIndex), npOutput[0])
    #saveAsImage('inputY{}.png'.format(imageIndex), npOutput[1])

    fileName = dataDir + str(uuid.uuid4())
    print("\tsaving in " + fileName + ".npz")
    np.savez_compressed(fileName, a=npOutput)


# main
for n in range(samples):
    print("Run {}:".format(n))

    files = os.listdir(airfoil_database)
    fileNumber = np.random.randint(0, len(files))

    print("\tusing {}".format(files[fileNumber]))

    if genMesh(airfoil_database + files[fileNumber]) != 0:
        print("\taborting");
        continue

    freestreamX = np.random.uniform(freestreamX_min, freestreamX_max)
    freestreamY = np.random.uniform(freestreamY_min, freestreamY_max)

    print("\tVel x: {}".format(freestreamX))
    print("\tVel y: {}".format(freestreamY))
    runSim(freestreamX, freestreamY)

    outputProcessing(freestreamX, freestreamY, imageIndex=n)
    print("\tdone")
