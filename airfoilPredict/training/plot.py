################
# plot.py
#
# Plots training curves of the last training run
################

import numpy as np
import matplotlib.pyplot as plt

L1 = np.loadtxt("L1.txt")
L1val = np.loadtxt("L1val.txt")

f, ax = plt.subplots(1)
ax.plot(L1)
ax.plot(L1val)
ax.set_xlabel('epoch')
ax.set_ylabel('L1 loss')
plt.savefig("visualization/L1loss.png")
