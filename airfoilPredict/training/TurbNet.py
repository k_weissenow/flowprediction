#!/usr/bin/python

import torch
import torch.nn as nn
import torch.nn.functional as F

def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

def blockUNet(in_c, out_c, name, transposed=False, bn=True, relu=True, dropout=False):
    block = nn.Sequential()
    if relu:
        block.add_module('%s.relu' % name, nn.ReLU(inplace=True))
    else:
        block.add_module('%s.leakyrelu' % name, nn.LeakyReLU(0.2, inplace=True))
    if not transposed:
        block.add_module('%s.conv' % name, nn.Conv2d(in_c, out_c, 4, 2, 1, bias=False))
    else:
        block.add_module('%s.tconv' % name, nn.ConvTranspose2d(in_c, out_c, 4, 2, 1, bias=False))
    if bn:
        block.add_module('%s.bn' % name, nn.BatchNorm2d(out_c))
    if dropout:
        block.add_module('%s.dropout' % name, nn.Dropout2d(0.5, inplace=True))
    return block

class TurbNetG(nn.Module):
    def __init__(self, channelExponent=6):
        super(TurbNetG, self).__init__()

        channels = 2 ** channelExponent

        self.layer1 = nn.Sequential()
        self.layer1.add_module('layer1.conv', nn.Conv2d(3, channels, 4, 2, 1, bias=False))

        self.layer2 = blockUNet(channels, channels*2, 'layer2', transposed=False, bn=True, relu=False, dropout=False)
        self.layer2x = blockUNet(channels*2, channels*2, 'layer2x', transposed=False, bn=True, relu=False, dropout=False)
        self.layer3 = blockUNet(channels*2, channels*4, 'layer3', transposed=False, bn=True, relu=False, dropout=False)
        self.layer4 = blockUNet(channels*4, channels*8, 'layer4', transposed=False, bn=True, relu=False, dropout=False)
        self.layer5 = blockUNet(channels*8, channels*8, 'layer5', transposed=False, bn=True, relu=False, dropout=False)
        self.layer6 = blockUNet(channels*8, channels*8, 'layer6', transposed=False, bn=False, relu=False, dropout=False)

        self.dlayer6 = blockUNet(channels*8, channels*8, 'dlayer6', transposed=True, bn=True, relu=True, dropout=True)
        self.dlayer5 = blockUNet(channels*16, channels*8, 'dlayer5', transposed=True, bn=True, relu=True, dropout=True)
        self.dlayer4 = blockUNet(channels*16, channels*4, 'dlayer4', transposed=True, bn=True, relu=True, dropout=True)
        self.dlayer3 = blockUNet(channels*8, channels*2, 'dlayer3', transposed=True, bn=True, relu=True, dropout=False)
        self.dlayer2x = blockUNet(channels*4, channels*2, 'dlayer2x', transposed=True, bn=True, relu=True, dropout=False)
        self.dlayer2 = blockUNet(channels*4, channels, 'dlayer2', transposed=True, bn=True, relu=True, dropout=False)

        self.dlayer1 = nn.Sequential()
        self.dlayer1.add_module('dlayer1.relu', nn.ReLU(inplace=True))
        self.dlayer1.add_module('dlayer1.tconv', nn.ConvTranspose2d(channels*2, 3, 4, 2, 1, bias=False))
        self.dlayer1.add_module('dlayer1.tanh', nn.Tanh())

    def forward(self, x):
        out1 = self.layer1(x)
        out2 = self.layer2(out1)
        out2x = self.layer2x(out2)
        out3 = self.layer3(out2x)
        out4 = self.layer4(out3)
        out5 = self.layer5(out4)
        out6 = self.layer6(out5)
        dout6 = self.dlayer6(out6)
        dout6_out5 = torch.cat([dout6, out5], 1)
        dout5 = self.dlayer5(dout6_out5)
        dout5_out4 = torch.cat([dout5, out4], 1)
        dout4 = self.dlayer4(dout5_out4)
        dout4_out3 = torch.cat([dout4, out3], 1)
        dout3 = self.dlayer3(dout4_out3)
        dout3_out2x = torch.cat([dout3, out2x], 1)
        dout2x = self.dlayer2x(dout3_out2x)
        dout2x_out2 = torch.cat([dout2x, out2], 1)
        dout2 = self.dlayer2(dout2x_out2)
        dout2_out1 = torch.cat([dout2, out1], 1)
        dout1 = self.dlayer1(dout2_out1)
        return dout1

