################
# validation.py
#
# Generates visualizations on the validation set based on a pre-trained model
################

import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt
import os
import random
from sys import maxsize
from TurbNet import TurbNetD, TurbNetG, weights_init
from TurbDataset import TurbDataset
from utils import imageOut, saveAsImage

netG = TurbNetG()
print(netG)
netG.load_state_dict(torch.load("modelG"))
netG.cuda()

dataset = TurbDataset(train=False)
testLoader = DataLoader(dataset, batch_size=1, shuffle=False)

targets = torch.FloatTensor(1, 3, 128, 128)
targets = Variable(targets)
targets = targets.cuda()
inputs = torch.FloatTensor(1, 3, 128, 128)
inputs = Variable(inputs)
inputs = inputs.cuda()

netG.eval()

for i, data in enumerate(testLoader, 0):
    inputs_cpu, targets_cpu = data
    current_batch_size = targets_cpu.size(0)

    targets_cpu, inputs_cpu = targets_cpu.float().cuda(), inputs_cpu.float().cuda()
    inputs.data.resize_as_(inputs_cpu).copy_(inputs_cpu)
    targets.data.resize_as_(targets_cpu).copy_(targets_cpu)

    outputs = netG(inputs)
    outputs_cpu = outputs.data.cpu().numpy()

    outputs_denormalized = dataset.denormalize(outputs_cpu[0])
    targets_denormalized = dataset.denormalize(targets_cpu.cpu().numpy()[0])
    imageOut("validation/{}".format(i), outputs_denormalized, targets_denormalized, saveTargets=True)


