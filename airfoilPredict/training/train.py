################
# train.py
#
# Main model training script
################

import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt
import os
import random
from sys import maxsize
from TurbNet import TurbNetD, TurbNetG, weights_init
from TurbDataset import TurbDataset
from utils import imageOut, saveAsImage

######## Settings ########
# amount of epochs
epochs = 400
# batch size
batch_size = 10
# L1 weight
lambda_L1 = 1.0
# learning rate, generator
lrG = 0.0006
# adam beta
beta = 0.5
# regularization strength
reg = 0.0
# enable cuda
cuda = True
##########################

def log(file, line):
    f = open(file, "a+")
    f.write(line)
    f.close()

losses = np.zeros(epochs)
validationAccuracies = np.zeros(epochs)
trainAccuracies = np.zeros(epochs)

seed = random.randint(0, maxsize)
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)
print("Seed: {}".format(seed))

netG = TurbNetG(channelExponent=6)
print(netG)
netG.apply(weights_init)
#netG.load_state_dict(torch.load("G_pretrain.model"))
netG.cuda()

dataset = TurbDataset()
trainLoader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
testLoader = DataLoader(TurbDataset(train=False), batch_size=batch_size, shuffle=False)
print("Training batches: {}".format(len(trainLoader)))
print("Test batches: {}".format(len(testLoader)))

criterionL1 = nn.L1Loss()
criterionL1.cuda()
optimizerG = optim.Adam(netG.parameters(), lr=lrG, betas=(beta, 0.999), weight_decay=0.0)

targets = torch.FloatTensor(batch_size, 3, 128, 128)
targets = Variable(targets)
targets = targets.cuda()
inputs = torch.FloatTensor(batch_size, 3, 128, 128)
inputs = Variable(inputs)
inputs = inputs.cuda()

L1_list = list()

for epoch in range(epochs):
    print("Starting epoch {}".format(epoch))

    netG.train()
    L1_accum = 0.0
    samples_accum = 0
    for i, data in enumerate(trainLoader, 0):
        inputs_cpu, targets_cpu = data
        current_batch_size = targets_cpu.size(0)

        targets_cpu, inputs_cpu = targets_cpu.float().cuda(), inputs_cpu.float().cuda()
        inputs.data.resize_as_(inputs_cpu).copy_(inputs_cpu)
        targets.data.resize_as_(targets_cpu).copy_(targets_cpu)

        netG.zero_grad()

        gen_out = netG(inputs)
        lossL1 = criterionL1(gen_out, targets)
        lossL1viz = lossL1.data[0]
        lossL1 = lossL1 * lambda_L1
        lossL1.backward()

        optimizerG.step()

        logline = "Epoch: {} Batch-idx: {} L1: {}\n".format(epoch, i, lossL1viz)
        print(logline)
        #log("log.txt", logline)

        L1_accum += lossL1viz
        samples_accum += current_batch_size

    # validation
    netG.eval()
    L1val_accum = 0.0
    for i, data in enumerate(testLoader, 0):
        inputs_cpu, targets_cpu = data
        current_batch_size = targets_cpu.size(0)

        targets_cpu, inputs_cpu = targets_cpu.float().cuda(), inputs_cpu.float().cuda()
        inputs.data.resize_as_(inputs_cpu).copy_(inputs_cpu)
        targets.data.resize_as_(targets_cpu).copy_(targets_cpu)

        outputs = netG(inputs)
        outputs_cpu = outputs.data.cpu().numpy()

        lossL1 = criterionL1(outputs, targets)
        L1val_accum += lossL1.data[0]

        if i==0:
            outputs_denormalized = dataset.denormalize(outputs_cpu[0])
            targets_denormalized = dataset.denormalize(targets_cpu.cpu().numpy()[0])
            imageOut("results/epoch{}_{}".format(epoch, i), outputs_denormalized, targets_denormalized, saveTargets=True)

    # plotting
    L1_accum /= len(trainLoader)
    log("L1.txt", "{} ".format(L1_accum))
    L1val_accum /= len(testLoader)
    log("L1val.txt", "{} ".format(L1val_accum))

torch.save(netG.state_dict(), "./modelG")
