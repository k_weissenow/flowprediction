from torch.utils.data import Dataset
import numpy as np
from os import listdir

class TurbDataset(Dataset):

    def __init__(self, train=True, overtrain=False, overtrainBatch=64, dataDir="../airfoil_generatedData/"):
        """
        :param train: boolean switch for training/test mode
        :param dataDir: directory containing training data
        """
        self.train = train
        self.dataDir = dataDir

        files = listdir(self.dataDir)
        self.totalLength = len(files)
        self.inputs = np.empty((len(files), 3, 128, 128))
        self.targets = np.empty((len(files), 3, 128, 128))
        for i, file in enumerate(files):
            npfile = np.load(dataDir + file)
            data = npfile['a']
            self.inputs[i] = data[0:3]
            self.targets[i] = data[3:6]

        self.minima = np.min(self.targets, axis=0)
        self.targets -= self.minima
        self.maxima = np.max(self.targets, axis=0)
        self.maxima[self.maxima == 0.0] = 1.0
        self.targets /= self.maxima
        self.targets -= 0.5
        self.targets *= 2.0

        self.input_minima = np.min(self.inputs, axis=0)
        self.inputs -= self.input_minima
        self.input_maxima = np.max(self.inputs, axis=0)
        self.input_maxima[self.input_maxima == 0.0] = 1.0
        self.inputs /= self.input_maxima
        self.inputs -= 0.5
        self.inputs *= 2.0

        if overtrain:
            self.totalLength = overtrainBatch
            self.inputs = self.inputs[:overtrainBatch]
            self.targets = self.targets[:overtrainBatch]
        else:
            # split for test/train set
            targetLength = len(files) * 0.8
            targetLength = int(targetLength)
            if train:
                self.inputs = self.inputs[:targetLength]
                self.targets = self.targets[:targetLength]
                self.totalLength = self.inputs.shape[0]
            else:
                self.inputs = self.inputs[targetLength:]
                self.targets = self.targets[targetLength:]
                self.totalLength = self.inputs.shape[0]

    def __len__(self):
        return self.totalLength

    def __getitem__(self, idx):
        return self.inputs[idx], self.targets[idx]

    # reverts normalization
    def denormalize(self, data):
        a = data.copy()
        a *= 0.5
        a += 0.5
        a *= self.maxima
        a += self.minima
        return a

    def denormalize_input(self, data):
        a = data.copy()
        a *= 0.5
        a += 0.5
        a *= self.input_maxima
        a += self.input_minima
        return a

