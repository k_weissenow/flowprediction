********************************************************************
* Files for flow prediction featuring airfoils
********************************************************************

* Data generation:
The directory 'OpenFOAM' contains the case files for the underlying OpenFOAM simulation. Simply run the script 'dataGen.py' to generate samples based on airfoils from the UIUC database in the directory 'airfoil_database'. 30 airfoil shapes have been seperated from the database in the directory 'airfoil_database_validation', which can be used to test the generalization performance of the trained model later.
Output files are saved as compressed numpy arrays. The tensor size in each sample file is 6x128x128. The first three channels represent the input, consisting of two fields corresponding to the freestream velocities in x and y direction and one field containing a mask of the airfoil geometry. The other three channels represent the target, containing one pressure and two velocity fields.

* Network training:
The main training script in the 'training' directory is 'train.py'. 'plot.py' is used to plot graphs based on the log files of the most recent training run. The script 'validation.py' is used to generate output images from the validation set based on a pre-trained model (which is automatically saved by the training script at the end of the training). To check the generalization performance on the seperated dataset, run 'validation_unseenAirfoils.py' instead.

